import java.util.Scanner;
public class ApplianceStore{
	
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		//Initialize an array of appliances
		AirFryer [] airFry = new AirFryer[4];
		
		//Input fields for each appliance
		for(int i=0; i < airFry.length; i++)
		{
		 airFry[i] = new AirFryer();
		 System.out.println("This is appliance number "+i);
         System.out.println("Enter the max degrees it can cook at :");		 
		 airFry[i].degrees = scan.nextInt();
		 System.out.println("Enter the max time it can cook for :");	
		 airFry[i].cookingTime = scan.nextInt();
		 System.out.println("Enter the color of the appliance :");	
		 airFry[i].color = scan.next();
		}
		//Print 3 fields of the last applience
		System.out.println("Max degrees: "+airFry[3].degrees+"  Max time: "+ airFry[3].cookingTime+" Color: "+airFry[3].color);
		
		//Print 2 methods
		airFry[0].maxCookingTime();
		airFry[0].maxDegrees();
		
	}
	
	
}